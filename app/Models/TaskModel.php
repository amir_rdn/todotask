<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskModel extends Model
{
    use HasFactory;

    protected $table    = 'task';
    protected $fillable = ['id', 'title', 'description', 'due_date', 'priority', 'tags', 'user_id', 'created_at', 'updated_at'];

    public function taskusers()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
