<?php

namespace App\Services;

use App\Models\TaskModel;
class TaskServices
{
    public function __construct()
    {
        //
    }
    public function sqlTask()
    {
        return TaskModel::query();
    }
    public function store(object $data)
    {
        $data->action               = 'create';

        return $this->insertOrUpdate($data);
    }
    public function update(object $data)
    {
        $data->action               = 'update';
        
        return $this->insertOrUpdate($data);
    }
    public function insertOrUpdate(object $data)
    {
        if($data->action == 'create'){
            $task                   = new TaskModel;
        }else if($data->action == 'update'){
            $task                   = TaskModel::find($data->task_id);
        }else{
            return response()->json([
                'success' => false,
                'message'   => 'No method actions'
            ], 400);
        }
        $task->title                = $data->title;
        $task->description          = $data->description;
        $task->due_date             = date('Y-m-d', strtotime($data->due_date));
        $task->priority             = $data->priority;
        $task->tags                 = !empty($data->tags) ? implode(",", $data->tags) : null;
        $task->user_id              = \Auth::user()->id;

        $task->save();

        return response()->json([
            'success'   => true,
            'message'   => 'Success '.$data->action,
            'data'  => $task
        ]);
    }
    public function delete(array $task_id)
    {
        $taskedentify               = TaskModel::whereIn('id', $task_id);

        if(count($taskedentify->get()) > 0){
            $taskedentify->delete();
        }else{
            return response()->json([
                'success'   => false,
                'message'   => 'Error delete'
            ], 400);
        }

        return response()->json([
            'success'   => true,
            'message'   => 'Success delete data!'
        ], 200);
    }
}
