<?php

namespace App\Services;

use App\Models\User;

class UserServices
{
    public function __construct()
    {
        //
    }
    public function usersql()
    {
        return User::from('users as u');
    }
    public function store(object $data)
    {
        $data->action = 'create';
        return $this->insertOrUpdate($data);
    }
    public function update(object $data)
    {
        $data->action = 'update';
        return $this->insertOrUpdate($data);
    }
    public function insertOrUpdate(object $data)
    {
        $checkusers =   User::where(\DB::raw("upper(name)"), strtoupper($data->name));

        if(count($checkusers->get()) > 0){
            return response()->json([
                'success' => false,
                'message'   => 'Username already exist'
            ], 400);
        }
        if($data->action == 'create'){
            $users              = new User;
        }else if($data->action == 'update'){
            $users              = User::find($data->user_id);
        }else{
            return response()->json([
                'success' => false,
                'message'   => 'no methods !'
            ], 400);
        }

        $users->name            = $data->name;
        $users->email           = $data->email;
        if($data->action == 'create'){
            $users->password    = bcrypt($data->password); 
        }
        if($data->action == 'update' && !empty($data->password)){
            $users->password    = bcrypt($data->password); 
        }
        $users->roles           = 2;

        $users->save();

        return response()->json([
            'success'   => true,
            'message'   => 'success '.$data->action,
            'data'  => $users
        ], 200);
        
    }
}
