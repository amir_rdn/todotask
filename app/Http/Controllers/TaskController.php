<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\TaskServices;
Use Exception;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        try{
            $task                   = (new TaskServices())->sqlTask()
                                    ->with('taskusers');
            if(\Auth::user()->roles != 1){
                $task               = $task->where('user_id', \Auth::user()->id);
            }
            if(!empty($request->priority)){
                $task               = $task->where('priority', $request->priority);
            }
            if(!empty($request->query_search)){
                $task               = $task->where('title', 'LIKE', '%'.$request->query_search.'%');
            }
            if(!empty($request->start_date) && !empty($request->end_date)){
                $task               = $task->whereBetween('due_date', [date('YYYY-mm-dd', strtotime($request->start_date)), date('YYYY-mm-dd', strtotime($request->end_date, '+ 1 day'))]);
            }
            $task                   = $task
                                    ->orderBy('updated_at', 'desc')
                                    ->paginate($request->per_page);
    
            return response()->json([
                'success'   => true,
                'message'   => 'Success',
                'data'  => $task
            ], 200);
        } catch(Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'title' => 'required',
            'due_date' => 'required',
            'priority' => 'required',
        ],[
            'title.required' => 'Title is required !',
            'due_date.required' => 'Due Date is required !',
            'priority.required' => 'Priority is required !'
        ]);
        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->json([
                'message'   => $error,
                'code'  => 400
            ]);
        }

        return (new TaskServices())->store($request);
    }
    public function view($task_id)
    {
        try{
            $task                   = (new TaskServices())->sqlTask()
                                    ->with('taskusers')
                                    ->find($task_id);
            return response()->json([
                'success'   => true,
                'message'   => 'Success',
                'data'  => $task
            ]);
        } catch(Exception $e) {
            return response()->json(['success' => true, 'message' => $e->getMessage()], 400);
        }
    }
    public function update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'title' => 'required',
            'due_date' => 'required',
            'priority' => 'required',
            'task_id'   => 'required'
        ],[
            'title.required' => 'Title is required !',
            'due_date.required' => 'Due Date is required !',
            'priority.required' => 'Priority is required !',
            'task_id.required' => 'Task is required !'
        ]);
        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->json([
                'message'   => $error,
                'code'  => 400
            ]);
        }

        return (new TaskServices())->update($request);
    }
    public function destroy(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'task_id' => 'required',
        ],[
            'task_id.required' => 'Task is required !'
        ]);
        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->json([
                'message'   => $error,
                'code'  => 400
            ]);
        }

        return (new TaskServices())->delete($request->task_id);
    }
}
