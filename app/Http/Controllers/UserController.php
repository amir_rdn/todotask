<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserServices;

use Validator;
use JWTFactory;
use JWTAuth;
use Auth;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password'=> 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'message'   => 'These credentials do not match our records.',
                ], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $getusers                   = (new UserServices())->usersql(\Auth::user()->id)
                                    ->select('u.*')
                                    ->first();
        $getusers->access_token = $token;
        
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'users' => $getusers
        ]);
    }
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
        ],[
            'name.required' => 'Username is required !',
            'email.required' => 'Email is required !'
        ]);
        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->json([
                'message'   => $error,
                'code'  => 400
            ]);
        }

        return (new UserServices())->store($request);
    }
    public function update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
        ],[
            'name.required' => 'Username is required !',
            'email.required' => 'Email is required !'
        ]);
        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->json([
                'message'   => $error,
                'code'  => 400
            ]);
        }

        return (new UserServices())->store($request);
    }
    public function Unauthenticated()
    {
        return response()->json([
            'message'   => 'Unauthenticated',
            'code'  => 401
        ]);
    }
}
