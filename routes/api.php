<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['api'])->group(function() {
    Route::post('/auth',[UserController::class, 'login']);
    Route::post('/users/register', [UserController::class, 'store']);
    Route::get('/unauthenticated',[UserController::class, 'unauthenticated'])->name('unauthenticated');
    Route::middleware('auth:api')->group(function() {
        Route::group(['prefix'=>'users'], function(){
           
            Route::post('/update', [UserController::class, 'update']);
        });
        Route::group(['prefix'=>'task'], function(){
            Route::get('/list', [TaskController::class, 'index']);
            Route::post('/store', [TaskController::class, 'store']);
            Route::put('/{task_id}', [TaskController::class, 'store']);
            Route::post('/update', [TaskController::class, 'update']);
            Route::delete('/delete', [TaskController::class, 'destroy']);
        });
    });
});