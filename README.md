
# Todo Task


## Tech Stack

**Client:** Vue 3, Vite, Bootstrap Vue 3, Primevue, 

**Server:** Laravel 10




## Installation

Install vue vite with npm

```bash
  cd my-project
  npm install
```

Install Laravel
```bash
  cd my-project
  composer install
``` 
## Migration Table
```bash
    php artisan migrate
```
## Seeder Table
```bash
    php artisan db:seed --class=UsersSeeder
```

## API Reference

#### Login

```http
  POST /api/auth
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `email` | `string` | **Required**. Your Email |
| `password` | `string` | **Required**. Your Passwrod |

#### Register Users

```http
  POST /api/users/register
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `name`      | `string` | **Required**. Usernamen |
| `email`      | `string` | **Required**. Email |
| `roles`      | `string` | **Required**. if 1 is admin and 2 member |

#### Get Task

```http
  GET /api/task/list?page=1&per_page=10&query_search=&priority=&due_date=
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `page`      | `integer` |  Current page |
| `per_page`      | `integer` |  Limit page |
| `query_search` | `string` |  filter title |
| `priority`  | `string` |  filter priority |
| `due_date`  | `date` |  filter due date |

#### Insert Task

```http
  POST /api/task/store
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `title`      | `string` |  **Required** |
| `description`      | `string` |  **Required** |
| `due_date` | `date` |  **Required** |
| `priority`   | `string` |  **Optional** |
| `tags`      | `array` |  **Optional** |


#### Update Task

```http
  POST /api/task/update
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `title`      | `string` |  **Required** |
| `description`      | `string` |  **Required** |
| `due_date` | `date` |  **Required** |
| `priority`   | `string` |  **Optional** |
| `tags`      | `array` |  **Optional** |
| `task_id`      | `integer` |  **Required** |

#### Delete Task

```http
  Delete /api/task/delete
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `task_id`      | `array` |  **Required** |