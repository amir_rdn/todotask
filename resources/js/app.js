import './bootstrap';
import BootstrapVue3 from "bootstrap-vue-3";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue-3/dist/bootstrap-vue-3.css";
import router from './router'
import store from "./store";
import PrimeVue from 'primevue/config';
// import "primevue/resources/primevue.min.css";
import 'primevue/resources/themes/lara-dark-teal/theme.css';
import VueDatePicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css'
import "primeicons/primeicons.css";
// import "primevue/resources/themes/saga-blue/theme.css";
import "primevue/resources/primevue.min.css";
import ConfirmationService from 'primevue/confirmationservice';
import ToastService from 'primevue/toastservice';

import {createApp} from 'vue'

import App from './App.vue'
const app = createApp(App);
app.component('VueDatePicker', VueDatePicker);
app.use(BootstrapVue3);
app.use(router)
app.use(store)
app.use(PrimeVue)
app.use(ConfirmationService)
app.use(ToastService)
app.mount("#app");